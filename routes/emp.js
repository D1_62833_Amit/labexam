const express = require('express')
const db = require('../db')
const utils = require('../utils')
const router = express.Router()

router.get('/', (request, response) => {
  const statement = `SELECT * FROM employee`

  db.pool.query(statement, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.post('/', (request, response) => {
  const { firstname, lastname, salary } = request.body
  const statement = `INSERT INTO employee
                 (firstname, lastname, salary) VALUES(?,?,?)`
  db.pool.query(statement, [firstname, lastname, salary], (error, result) => {
    response.send(utils.createResult(error, result))
  })
})
module.exports = router
