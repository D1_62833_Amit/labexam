const express = require('express')
const empRouter = require('./routes/emp')
const app = express()
app.use(express.json())
app.use('/emp', empRouter)

app.listen(4000, '0.0.0.0', () => {
  console.log('Server is started on port 4000')
  process.exit()
})
