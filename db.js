const mysql = require('mysql')

const pool = mysql.createPool({
  //this host is used in yaml file
  host: 'database',

  user: 'root',
  password: 'root',
  database: 'mydb',
  port: 3306,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
})

module.exports = {
  pool,
}
